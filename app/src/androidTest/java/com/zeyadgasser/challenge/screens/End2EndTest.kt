package com.zeyadgasser.challenge.screens

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.zeyad.gadapter.GenericRecyclerViewAdapter
import com.zeyadgasser.challenge.OkHttpIdlingResourceRule
import com.zeyadgasser.challenge.R
import com.zeyadgasser.challenge.RecyclerViewItemCountAssertion.Companion.withItemCount
import com.zeyadgasser.challenge.screens.items.master.MasterActivity
import com.zeyadgasser.challenge.utils.MOVIES
import io.appflate.restmock.RESTMockServer
import io.appflate.restmock.utils.RequestMatchers.pathEndsWith
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.IsInstanceOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class End2EndTest {

    @get:Rule
    var activityTestRule = ActivityTestRule(MasterActivity::class.java)

    @get:Rule
    var okHttpIdlingResourceRule = OkHttpIdlingResourceRule()

    private val urlPart = MOVIES.format(1)
    private val urlPart2 = MOVIES.format(2)

    private val listJson = "list.json"
    private val list2Json = "list2.json"

    private fun childAtPosition(parentMatcher: Matcher<View>, position: Int): Matcher<View> {
        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                parentMatcher.describeTo(description.appendText("Child at position $position in parent "))
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return (parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position))
            }
        }
    }

    @Before
    fun before() {
        RESTMockServer.reset()
    }

    @Test
    fun initialRun() {
        RESTMockServer.whenGET(pathEndsWith(urlPart)).thenReturnFile(listJson)
        Thread.sleep(500)
        onView(withId(R.id.master_list)).check(matches(isDisplayed()))
        onView(withId(R.id.master_list)).check(withItemCount(20))
    }

    @Test
    fun pagination() {
        RESTMockServer.whenGET(pathEndsWith(urlPart)).thenReturnFile(listJson)

        onView(withId(R.id.master_list))
                .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(19))

        RESTMockServer.whenGET(pathEndsWith(urlPart2)).thenReturnFile(list2Json)

        onView(withId(R.id.master_list))
                .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(5))
        onView(withId(R.id.master_list))
                .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(35))
    }

    @Test
    fun masterActivityTest() {
        RESTMockServer.whenGET(pathEndsWith(urlPart)).thenReturnFile(listJson)
        Thread.sleep(500)
        onView(ViewMatchers.withId(R.id.master_list))
                .perform(RecyclerViewActions.actionOnItemAtPosition<GenericRecyclerViewAdapter
                .GenericViewHolder<*>>(0, click()))


        val imageView = onView(allOf(withId(R.id.avatar), withContentDescription("Non accessible view"),
                childAtPosition(allOf(withId(R.id.rl_row_item),
                        childAtPosition(IsInstanceOf.instanceOf(android.widget.FrameLayout::class.java),
                                0)), 0), isDisplayed()))
        imageView.check(matches(isDisplayed()))
    }
}
