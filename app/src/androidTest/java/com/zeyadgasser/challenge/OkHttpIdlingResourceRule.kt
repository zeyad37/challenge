package com.zeyadgasser.challenge

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.IdlingRegistry
import com.jakewharton.espresso.OkHttp3IdlingResource
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class OkHttpIdlingResourceRule : TestRule {
    override fun apply(base: Statement, description: Description): Statement {
        val app = InstrumentationRegistry.getTargetContext().applicationContext as TestApp
        return object : Statement() {
            @Throws(Throwable::class)
            override fun evaluate() {
                val idlingResource = OkHttp3IdlingResource.create("okhttp", app.getOkHttpBuilder().build())
                val idlingRegistry = IdlingRegistry.getInstance()
                idlingRegistry.register(idlingResource)
                base.evaluate()
                idlingRegistry.unregister(idlingResource)
            }
        }
    }
}