package com.zeyadgasser.challenge

import io.appflate.restmock.RESTMockServer

class TestApp : App() {
    override fun getBaseURL(): String {
        return RESTMockServer.getUrl()
    }
}
