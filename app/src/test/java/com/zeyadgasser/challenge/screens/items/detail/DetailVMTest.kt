package com.zeyadgasser.challenge.screens.items.detail

import com.zeyad.usecases.api.IDataService
import com.zeyadgasser.challenge.screens.items.entities.ListResponse
import io.reactivex.Flowable
import io.reactivex.subscribers.TestSubscriber
import junit.framework.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Matchers
import org.mockito.Mockito
import org.mockito.Mockito.mock

/**
 * @author ZIaDo on 7/31/18.
 */
class DetailVMTest {

    private lateinit var dataService: IDataService
    private lateinit var detailVM: DetailVM

    @Before
    @Throws(Exception::class)
    fun setUp() {
        dataService = mock(IDataService::class.java)
        detailVM = DetailVM(dataService)
    }

    @Test
    fun getDetailList() {
        val listResponse = ListResponse()
        Mockito.`when`<Flowable<ListResponse>>(dataService.getObjectOffLineFirst<ListResponse>(Matchers.any()))
                .thenReturn(Flowable.just(listResponse))

        val testSubscriber = TestSubscriber<Any>()
        detailVM.getDetailList(1).subscribe(testSubscriber)
        testSubscriber.awaitTerminalEvent()

        testSubscriber.assertSubscribed()
                .assertNoErrors()
                .assertValueCount(1)
                .assertValue(listResponse)
                .assertComplete()
    }

    @Test
    fun reduceFromEmptyToGetState() {
        Assert.assertEquals(DetailState().detailList,
                detailVM.stateReducer().reduce(ListResponse(), "", DetailState()).detailList)
    }

    @Test(expected = IllegalStateException::class)
    fun reduceFromGetToException() {
        detailVM.stateReducer().reduce("", "", DetailState())
    }
}
