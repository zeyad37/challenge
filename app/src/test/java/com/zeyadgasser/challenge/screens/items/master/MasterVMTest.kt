package com.zeyadgasser.challenge.screens.items.master

import android.support.v7.util.DiffUtil
import com.zeyad.usecases.api.IDataService
import com.zeyadgasser.challenge.screens.items.entities.ListResponse
import io.reactivex.Flowable
import io.reactivex.subscribers.TestSubscriber
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Matchers.any
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

/**
 * @author ZIaDo on 7/30/18.
 */
class MasterVMTest {
    private lateinit var dataService: IDataService
    private lateinit var masterVM: MasterVM
    private val callback = DiffUtil.calculateDiff(MasterDiffCallBack(mutableListOf(), mutableListOf()))

    @Before
    @Throws(Exception::class)
    fun setUp() {
        dataService = mock(com.zeyad.usecases.api.IDataService::class.java)
        masterVM = MasterVM(dataService)
    }

    @Test
    fun getPagedList() {
        val listResponse = ListResponse()
        `when`<Flowable<ListResponse>>(dataService.getObjectOffLineFirst<ListResponse>(any()))
                .thenReturn(Flowable.just(listResponse))

        val testSubscriber = TestSubscriber<Any>()
        masterVM.getPagedList(1).subscribe(testSubscriber)
        testSubscriber.awaitTerminalEvent()

        testSubscriber.assertSubscribed()
                .assertNoErrors()
                .assertValueCount(1)
                .assertValue(listResponse)
                .assertComplete()
    }

    @Test
    fun reduceFromEmptyToGetState() {
        assertEquals(GetState(gCallback = callback).page,
                (masterVM.stateReducer().reduce(ListResponse(), "", EmptyState(1)) as
                        GetState).page)
    }

    @Test
    fun reduceFromGetToGetState() {
        assertEquals(GetState(gPage = 2, gCallback = callback).page,
                (masterVM.stateReducer().reduce(ListResponse(), "", GetState(gCallback =
                callback)) as GetState).page)
    }

    @Test(expected = IllegalStateException::class)
    fun reduceFromEmptyToException() {
        masterVM.stateReducer().reduce("", "", EmptyState())
    }

    @Test(expected = IllegalStateException::class)
    fun reduceFromGetToException() {
        masterVM.stateReducer().reduce("", "", GetState())
    }
}