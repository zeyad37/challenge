package com.zeyadgasser.challenge

import android.app.Application
import android.util.Log
import com.zeyadgasser.challenge.di.myModule
import com.zeyadgasser.challenge.utils.API_BASE_URL
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.rx.RealmObservableFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.android.startKoin
import java.util.concurrent.TimeUnit

/**
 * @author ZIaDo on 7/19/18.
 */
open class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initializeRealm()
        startKoin(this, listOf(myModule))
    }

    fun getOkHttpBuilder(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor { Log.d(NETWORK_INFO, it) }
                        .setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                        else HttpLoggingInterceptor.Level.NONE))
                .connectTimeout(TIME_OUT.toLong(), TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT.toLong(), TimeUnit.SECONDS)
                .readTimeout(TIME_OUT.toLong(), TimeUnit.SECONDS)
    }

    open fun getBaseURL() = API_BASE_URL

    private fun initializeRealm() {
        Realm.init(this)
        Realm.setDefaultConfiguration(RealmConfiguration.Builder()
                .name("app.realm")
                .modules(Realm.getDefaultModule()!!, LibraryModule())
                .rxFactory(RealmObservableFactory())
                .deleteRealmIfMigrationNeeded()
                .build())
    }

    companion object {
        private const val TIME_OUT = 15
        private const val NETWORK_INFO = "NetworkInfo"
    }
}
