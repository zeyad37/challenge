package com.zeyadgasser.challenge.screens.items

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.zeyad.gadapter.GenericRecyclerViewAdapter
import com.zeyadgasser.challenge.screens.items.entities.ListItem
import com.zeyadgasser.challenge.utils.PHOTOS_BASE_URL
import kotlinx.android.synthetic.main.item_list_content.view.*

class ItemViewHolder(itemView: View) : GenericRecyclerViewAdapter.GenericViewHolder<ListItem>(itemView) {

    override fun bindData(item: ListItem, isItemSelected: Boolean, position: Int, isEnabled: Boolean) {
        itemView.avatar?.let {
            Glide.with(itemView.context)
                    .load("${PHOTOS_BASE_URL}500/${item.poster_path}")
                    .into(it)
            itemView.title.text = item.name
            itemView.vote.text = item.vote_average.toString()
        }
    }

    fun getAvatar(): ImageView = itemView.avatar
    fun getTextViewTitle(): TextView = itemView.title
}