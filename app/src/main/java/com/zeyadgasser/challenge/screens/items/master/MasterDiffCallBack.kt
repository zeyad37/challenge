package com.zeyadgasser.challenge.screens.items.master

import android.support.v7.util.DiffUtil
import com.zeyad.gadapter.ItemInfo
import com.zeyadgasser.challenge.screens.items.entities.ListItem

/**
 * @author ZIaDo on 12/13/17.
 */
class MasterDiffCallBack(private var newList: List<ItemInfo>, private var oldList: List<ItemInfo>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].getData<ListItem>() == newList[newItemPosition].getData<ListItem>()
    }
}
