package com.zeyadgasser.challenge.screens.items.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class ListResponse(var page: Int = 0,
                        @SerializedName("total_pages") var totalPages: Int = 0,
                        var results: RealmList<ListItem> = RealmList(),
                        @PrimaryKey var total_results: Int = 0) : RealmModel, Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            RealmList(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(page)
        parcel.writeInt(totalPages)
        parcel.writeInt(total_results)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<ListResponse> {
        override fun createFromParcel(parcel: Parcel) = ListResponse(parcel)

        override fun newArray(size: Int) = arrayOfNulls<ListResponse?>(size)
    }
}
