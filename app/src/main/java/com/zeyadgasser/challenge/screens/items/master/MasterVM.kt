package com.zeyadgasser.challenge.screens.items.master

import android.support.v7.util.DiffUtil
import com.zeyad.gadapter.ItemInfo
import com.zeyad.usecases.api.IDataService
import com.zeyad.usecases.requests.GetRequest
import com.zeyadgasser.challenge.R
import com.zeyadgasser.challenge.mvi.BaseEvent
import com.zeyadgasser.challenge.mvi.viewmodel.BaseViewModel
import com.zeyadgasser.challenge.mvi.viewmodel.StateReducer
import com.zeyadgasser.challenge.screens.items.entities.ListResponse
import com.zeyadgasser.challenge.utils.MOVIES
import io.reactivex.Flowable
import io.reactivex.functions.Function

class MasterVM(private val dataUseCase: IDataService) : BaseViewModel<ListState>() {

    override fun mapEventsToActions(): Function<BaseEvent<*>, Flowable<*>> {
        return Function { event ->
            when (event) {
                is GetListEvent -> getPagedList(event.getPayLoad())
                else -> Flowable.empty<Any>()
            }
        }
    }

    override fun stateReducer(): StateReducer<ListState> {
        return object : StateReducer<ListState> {
            override fun reduce(newResult: Any, event: String, currentStateBundle: ListState): ListState {
                val currentItemInfo = currentStateBundle.list.toMutableList()
                return when (currentStateBundle) {
                    is EmptyState -> when (newResult) {
                        is ListResponse -> {
                            val pair = Flowable.fromIterable(newResult.results)
                                    .map { ItemInfo(it, R.layout.item_list_content).setId(it.id.toLong()) }
                                    .toList()
                                    .toFlowable()
                                    .scan<Pair<MutableList<ItemInfo>, DiffUtil.DiffResult>>(Pair(currentItemInfo,
                                            DiffUtil.calculateDiff(MasterDiffCallBack(mutableListOf(),
                                                    mutableListOf()))))
                                    { pair1, next ->
                                        Pair(next, DiffUtil.calculateDiff(MasterDiffCallBack(pair1.first, next)))
                                    }
                                    .skip(1)
                                    .blockingFirst()
                            GetState(pair.first, gCallback = pair.second, gPage = currentStateBundle.page)
                        }
                        else -> throw IllegalStateException("Can not reduce EmptyState with this result: $newResult!")
                    }
                    is GetState -> when (newResult) {
                        is ListResponse -> {
                            currentItemInfo.addAll(Flowable.fromIterable(newResult.results)
                                    .map { ItemInfo(it, R.layout.item_list_content).setId(it.id.toLong()) }
                                    .toList().blockingGet())
                            val pair = Flowable.just(currentItemInfo.toSet().toMutableList())
                                    .scan<Pair<MutableList<ItemInfo>, DiffUtil.DiffResult>>(Pair(currentItemInfo,
                                            DiffUtil.calculateDiff(MasterDiffCallBack(mutableListOf(),
                                                    mutableListOf()))))
                                    { pair1, next ->
                                        Pair(next, DiffUtil.calculateDiff(MasterDiffCallBack(pair1.first, next)))
                                    }
                                    .skip(1)
                                    .blockingFirst()
                            GetState(gList = pair.first, gCallback = pair.second,
                                    gPage = currentStateBundle.page + 1)
                        }
                        else -> throw IllegalStateException("Can not reduce GetState with this result: $newResult!")
                    }
                }
            }
        }
    }

    fun getPagedList(currentPage: Int): Flowable<ListResponse> {
        return dataUseCase.getObjectOffLineFirst<ListResponse>(GetRequest
                .Builder(ListResponse::class.java, true)
                .url(MOVIES.format(currentPage))
                .id(20021, "total_results", Int::class.java)
                .build())
    }
}
