package com.zeyadgasser.challenge.screens.items.detail

import com.zeyadgasser.challenge.mvi.BaseEvent

/**
 * @author ZIaDo on 7/31/18.
 */
class GetSimilarEvent(val id: Int) : BaseEvent<Int> {
    override fun getPayLoad(): Int {
        return id
    }
}