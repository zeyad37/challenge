package com.zeyadgasser.challenge.screens.items.detail

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andrognito.flashbar.Flashbar
import com.bumptech.glide.Glide
import com.zeyad.gadapter.GenericRecyclerViewAdapter
import com.zeyadgasser.challenge.R
import com.zeyadgasser.challenge.mvi.BaseEvent
import com.zeyadgasser.challenge.mvi.view.BaseFragment
import com.zeyadgasser.challenge.mvi.view.BaseView.UI_MODEL
import com.zeyadgasser.challenge.mvi.view.ErrorMessageFactory
import com.zeyadgasser.challenge.screens.items.ItemViewHolder
import com.zeyadgasser.challenge.screens.items.entities.ListItem
import com.zeyadgasser.challenge.ui.ItemOffsetDecoration
import com.zeyadgasser.challenge.utils.PHOTOS_BASE_URL
import com.zeyadgasser.challenge.utils.hasM
import com.zeyadgasser.challenge.utils.hasO
import com.zeyadgasser.challenge.utils.showErrorFlashBar
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.detail.*
import org.koin.android.architecture.ext.getViewModel


class DetailFragment : BaseFragment<DetailState, DetailVM>() {

    private var eventObservable: Observable<BaseEvent<*>> = Observable.empty()
    private val postOnResumeEvents by lazy { PublishSubject.create<BaseEvent<*>>() }
    private lateinit var adapter: GenericRecyclerViewAdapter
    private lateinit var item: ListItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (hasO()) {
            postponeEnterTransition()
        } else if (hasM()) {
            sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        }
        arguments?.let {
            if (it.containsKey(UI_MODEL)) {
                item = it.getParcelable(UI_MODEL)
                activity?.let {
                    it.toolbar_layout?.title = item.name
                    Glide.with(this@DetailFragment)
                            .load("${PHOTOS_BASE_URL}500/${item.poster_path}")
                            .into(it.iv_item_detail)
                }
            }
        }
    }

    /**
     * Initialize any objects or any required dependencies.
     */
    override fun initialize() {
        viewModel = getViewModel()
        eventObservable = Observable.just(GetSimilarEvent((arguments?.getParcelable(UI_MODEL) as ListItem).id))
        viewState = DetailState()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tv_overview.text = item.overview
        tv_vote.text = getString(R.string.vote_average, item.vote_average.toString())
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        context?.let {
            adapter = object : GenericRecyclerViewAdapter(
                    it.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater) {
                override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder<*> {
                    return when (viewType) {
                        R.layout.item_list_content -> ItemViewHolder(layoutInflater
                                .inflate(R.layout.item_list_content, parent, false))
                        else -> throw IllegalArgumentException("Could not find view of type $viewType")
                    }
                }
            }
            adapter.setAreItemsClickable(true)
            val layoutManager = LinearLayoutManager(activity)
            layoutManager.orientation = LinearLayoutManager.HORIZONTAL
            rv_similar.layoutManager = layoutManager
            rv_similar.adapter = adapter
            rv_similar.addItemDecoration(ItemOffsetDecoration(it, R.dimen.four_dp))
        }
    }

    override fun errorMessageFactory() = object : ErrorMessageFactory {
        override fun getErrorMessage(throwable: Throwable): String {
            return throwable.localizedMessage
        }
    }

    /**
     * Merge all events into one [Observable].
     *
     * @return [Observable].
     */
    override fun events(): Observable<BaseEvent<*>> = eventObservable.mergeWith(postOnResumeEvents)

    /**
     * Renders the model of the view
     *
     * @param successState the model to be rendered.
     */
    override fun renderSuccessState(successState: DetailState, event: String) {
        adapter.dataList = successState.detailList
        adapter.notifyDataSetChanged()
    }

    /**
     * Show or hide a view with a progress bar indicating a loading process.
     *
     * @param isLoading whether to show or hide the loading view.
     */
    override fun toggleViews(isLoading: Boolean, event: String) {
        loadLayout.bringToFront()
        loadLayout.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    /**
     * Show an errorResult message
     *
     * @param errorMessage A string representing an errorResult.
     */
    override fun showError(errorMessage: String, event: String) {
        activity?.showErrorFlashBar(errorMessage)
                ?.backgroundColorRes(android.R.color.holo_red_dark)
                ?.primaryActionText(R.string.retry)
                ?.primaryActionTextColor(R.color.white)
                ?.primaryActionTapListener(object : Flashbar.OnActionTapListener {
                    override fun onActionTapped(bar: Flashbar) {
                        bar.dismiss()
                        postOnResumeEvents.onNext(GetSimilarEvent((arguments?.getParcelable(UI_MODEL) as ListItem).id))
                    }
                })?.show()
    }

    companion object {

        fun newInstance(item: ListItem): DetailFragment {
            val detailFragment = DetailFragment()
            val bundle = Bundle()
            bundle.putParcelable(UI_MODEL, item)
            detailFragment.arguments = bundle
            return detailFragment
        }
    }
}


