package com.zeyadgasser.challenge.screens.items.master

import android.app.ActivityOptions
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andrognito.flashbar.Flashbar
import com.jakewharton.rxbinding2.support.v7.widget.RxRecyclerView
import com.zeyad.gadapter.GenericRecyclerViewAdapter
import com.zeyadgasser.challenge.R
import com.zeyadgasser.challenge.mvi.BaseEvent
import com.zeyadgasser.challenge.mvi.view.BaseActivity
import com.zeyadgasser.challenge.mvi.view.ErrorMessageFactory
import com.zeyadgasser.challenge.screens.items.ItemViewHolder
import com.zeyadgasser.challenge.screens.items.detail.DetailActivity
import com.zeyadgasser.challenge.screens.items.entities.ListItem
import com.zeyadgasser.challenge.ui.ItemOffsetDecoration
import com.zeyadgasser.challenge.ui.ScrollEventCalculator
import com.zeyadgasser.challenge.utils.FIRED
import com.zeyadgasser.challenge.utils.hasLollipop
import com.zeyadgasser.challenge.utils.showErrorFlashBar
import io.reactivex.Observable
import io.reactivex.internal.schedulers.IoScheduler
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_list.*
import kotlinx.android.synthetic.main.list.*
import kotlinx.android.synthetic.main.view_loader_layout.*
import org.koin.android.architecture.ext.getViewModel
import java.util.*
import java.util.concurrent.TimeUnit

class MasterActivity : BaseActivity<ListState, MasterVM>() {

    lateinit var adapter: GenericRecyclerViewAdapter
    private var eventObservable: Observable<BaseEvent<*>> = Observable.empty()
    private val postOnResumeEvents by lazy { PublishSubject.create<BaseEvent<*>>() }

    override fun errorMessageFactory(): ErrorMessageFactory = object : ErrorMessageFactory {
        override fun getErrorMessage(throwable: Throwable) = throwable.localizedMessage
    }

    override fun initialize() {
        viewModel = getViewModel()
        viewState = viewState ?: EmptyState()
        eventObservable = Observable.just<BaseEvent<*>>(GetListEvent(1))
                .doOnNext { Log.d("GetPaginatedListEvent", FIRED) }
    }

    override fun setupUI(isNew: Boolean) {
        setContentView(R.layout.activity_list)
        setSupportActionBar(toolbar)
        toolbar.title = title
        setupRecyclerView()
    }

    override fun events(): Observable<BaseEvent<*>> = eventObservable.mergeWith(postOnResumeEvents)

    override fun renderSuccessState(successState: ListState, event: String) {
        adapter.setDataList(successState.list, successState.callback)
    }

    override fun toggleViews(isLoading: Boolean, event: String) {
        linear_layout_loader.bringToFront()
        linear_layout_loader.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    override fun showError(errorMessage: String, event: String) {
        showErrorFlashBar(errorMessage)
                .backgroundColorRes(android.R.color.holo_red_dark)
                .primaryActionText(R.string.retry)
                .primaryActionTextColor(R.color.white)
                .primaryActionTapListener(object : Flashbar.OnActionTapListener {
                    override fun onActionTapped(bar: Flashbar) {
                        bar.dismiss()
                        postOnResumeEvents.onNext(GetListEvent(viewState?.page!!))
                    }
                })
                .show()
    }

    private fun setupRecyclerView() {
        adapter = object : GenericRecyclerViewAdapter(
                getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater, ArrayList()) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
                    when (viewType) {
                        R.layout.item_list_content -> ItemViewHolder(layoutInflater
                                .inflate(R.layout.item_list_content, parent, false))
                        else -> throw IllegalArgumentException("Could not find view of type $viewType")
                    }
        }
        adapter.setAreItemsClickable(true)
        adapter.setOnItemClickListener { _, itemInfo, holder ->
            val data = itemInfo.getData<Any>()
            if (data is ListItem) {
                if (hasLollipop()) {
                    val masterViewHolder = holder as ItemViewHolder
                    val avatar = masterViewHolder.getAvatar()
                    val pair: Pair<View, String> = Pair.create(avatar, avatar.transitionName)
                    val textViewTitle = masterViewHolder.getTextViewTitle()
                    val secondPair: Pair<View, String> = Pair.create(textViewTitle, textViewTitle
                            .transitionName)
                    val optionsBundle = ActivityOptions.makeSceneTransitionAnimation(this, pair,
                            secondPair).toBundle()
                    startActivity(DetailActivity.getCallingIntent(this, data), optionsBundle)
                } else {
                    startActivity(DetailActivity.getCallingIntent(this, data))
                }
            }
        }
        master_list.layoutManager = LinearLayoutManager(this)
        master_list.adapter = adapter
        master_list.addItemDecoration(ItemOffsetDecoration(this, R.dimen.four_dp))
        eventObservable = eventObservable.mergeWith(RxRecyclerView.scrollEvents(master_list)
                .map {
                    GetListEvent(if (ScrollEventCalculator.isAtScrollEnd(it)) viewState?.page!! + 1 else -1)
                }
                .filter { it.getPayLoad() != -1 }
                .throttleLast(200, TimeUnit.MILLISECONDS, IoScheduler())
                .debounce(300, TimeUnit.MILLISECONDS, IoScheduler())
                .doOnNext { Log.d("NextPageEvent", FIRED) })
    }
}
