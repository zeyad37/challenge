package com.zeyadgasser.challenge.screens.items.master

import android.os.Parcel
import android.os.Parcelable
import android.support.v7.util.DiffUtil
import com.zeyad.gadapter.ItemInfo
import kotlinx.android.parcel.Parcelize

sealed class ListState(val list: List<ItemInfo> = emptyList(),
                       val page: Int = 1,
                       val callback: DiffUtil.DiffResult =
                               DiffUtil.calculateDiff(MasterDiffCallBack(mutableListOf(), mutableListOf()))) : Parcelable

@Parcelize
data class EmptyState(val ePage: Int = 1) : ListState(), Parcelable

data class GetState(val gList: List<ItemInfo> = emptyList(),
                    val gPage: Int = 1,
                    val gCallback: DiffUtil.DiffResult =
                            DiffUtil.calculateDiff(MasterDiffCallBack(mutableListOf(), mutableListOf())))
    : ListState(gList, gPage, gCallback), Parcelable {

    constructor(parcel: Parcel) : this(emptyList(),
            parcel.readInt(),
            DiffUtil.calculateDiff(MasterDiffCallBack(mutableListOf<ItemInfo>(), mutableListOf<ItemInfo>())))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(gPage)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<GetState> {
        override fun createFromParcel(parcel: Parcel) = GetState(parcel)

        override fun newArray(size: Int) = arrayOfNulls<GetState?>(size)
    }
}
