package com.zeyadgasser.challenge.screens.items.detail

import android.os.Parcel
import android.os.Parcelable
import com.zeyad.gadapter.ItemInfo

data class DetailState(val detailList: List<ItemInfo> = emptyList()) : Parcelable {
    constructor(parcel: Parcel) : this(emptyList())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<DetailState> {
        override fun createFromParcel(parcel: Parcel) = DetailState(parcel)

        override fun newArray(size: Int) = arrayOfNulls<DetailState?>(size)
    }
}
