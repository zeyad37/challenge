package com.zeyadgasser.challenge.screens.items.master

import com.zeyadgasser.challenge.mvi.BaseEvent

data class GetListEvent(private val page: Int) : BaseEvent<Int> {
    override fun getPayLoad(): Int {
        return page
    }
}
