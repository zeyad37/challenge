package com.zeyadgasser.challenge.screens.items.detail

import com.zeyad.gadapter.ItemInfo
import com.zeyad.usecases.api.IDataService
import com.zeyad.usecases.requests.GetRequest
import com.zeyadgasser.challenge.R
import com.zeyadgasser.challenge.mvi.BaseEvent
import com.zeyadgasser.challenge.mvi.viewmodel.BaseViewModel
import com.zeyadgasser.challenge.mvi.viewmodel.StateReducer
import com.zeyadgasser.challenge.screens.items.entities.ListItem
import com.zeyadgasser.challenge.screens.items.entities.ListResponse
import com.zeyadgasser.challenge.utils.SIMILAR
import io.reactivex.Flowable
import io.reactivex.functions.Function

class DetailVM(private val dataUseCase: IDataService) : BaseViewModel<DetailState>() {
    override fun stateReducer(): StateReducer<DetailState> {
        return object : StateReducer<DetailState> {
            override fun reduce(newResult: Any, event: String, currentStateBundle: DetailState): DetailState {
                return when (newResult) {
                    is ListResponse -> {
                        DetailState(detailList = Flowable.fromIterable<ListItem>(newResult.results)
                                .map { ItemInfo(it, R.layout.item_list_content).setId(it.id.toLong()) }
                                .toList().blockingGet())
                    }
                    else -> throw IllegalStateException("Can not reduce DetailState with this result: $newResult!")
                }
            }
        }
    }

    override fun mapEventsToActions(): Function<BaseEvent<*>, Flowable<*>> {
        return Function { event ->
            when (event) {
                is GetSimilarEvent -> getDetailList(event.getPayLoad())
                else -> Flowable.empty<Any>()
            }
        }
    }

    fun getDetailList(id: Int): Flowable<ListResponse> {
        return dataUseCase.getObjectOffLineFirst(GetRequest
                .Builder(ListResponse::class.java, true)
                .url(SIMILAR.format(id, 1))
                .id(1406, "total_results", Int::class.java)
                .build())
    }
}
