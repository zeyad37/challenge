package com.zeyadgasser.challenge.screens.items.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.zeyadgasser.challenge.R
import com.zeyadgasser.challenge.mvi.view.BaseView.UI_MODEL
import com.zeyadgasser.challenge.screens.items.entities.ListItem
import com.zeyadgasser.challenge.screens.items.master.MasterActivity
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(detail_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.detail_container, DetailFragment.newInstance(intent.getParcelableExtra(UI_MODEL)))
                    .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    navigateUpTo(Intent(this, MasterActivity::class.java))
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    companion object {
        fun getCallingIntent(context: Context, detailModel: ListItem): Intent {
            return Intent(context, DetailActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra(UI_MODEL, detailModel)
        }
    }
}
