package com.zeyadgasser.challenge.screens.items.entities

import android.os.Parcelable
import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@RealmClass
open class ListItem(var poster_path: String = "",
                    var vote_average: Double = 0.0,
                    var overview: String = "",
                    var name: String = "",
                    @PrimaryKey var id: Int = 0) : RealmModel, Parcelable