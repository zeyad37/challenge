package com.zeyadgasser.challenge.di

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.zeyad.usecases.api.DataServiceConfig
import com.zeyad.usecases.api.DataServiceFactory
import com.zeyad.usecases.api.IDataService
import com.zeyadgasser.challenge.BuildConfig
import com.zeyadgasser.challenge.screens.items.detail.DetailVM
import com.zeyadgasser.challenge.screens.items.master.MasterVM
import com.zeyadgasser.challenge.utils.API_BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext
import java.util.concurrent.TimeUnit

val myModule: Module = applicationContext {
    viewModel { MasterVM(get()) }
    viewModel { DetailVM(get()) }
    bean { createDataService(get()) }
    bean { Gson() }
}

fun createDataService(context: Context): IDataService {
    DataServiceFactory.init(DataServiceConfig.Builder(context)
            .baseUrl(API_BASE_URL)
            .okHttpBuilder(getOkHttpBuilder())
            .withRealm()
            .build())
    return DataServiceFactory.getInstance()!!
}

fun getOkHttpBuilder(): OkHttpClient.Builder {
    return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor { Log.d("NetworkInfo", it) }
                    .setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                    else HttpLoggingInterceptor.Level.NONE))
            .connectTimeout(15L, TimeUnit.SECONDS)
            .writeTimeout(15L, TimeUnit.SECONDS)
            .readTimeout(15L, TimeUnit.SECONDS)
}
