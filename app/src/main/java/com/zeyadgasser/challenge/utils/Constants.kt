package com.zeyadgasser.challenge.utils

/**
 * @author ZIaDo on 7/19/18.
 */

const val PHOTOS_BASE_URL = "https://image.tmdb.org/t/p/w"
const val API_KEY = "93aea0c77bc168d8bbce3918cefefa45"
const val API_BASE_URL = "http://api.themoviedb.org/3/tv/"
const val QUERY = "api_key=$API_KEY&language=en-US&page=%s"
const val MOVIES = "popular?$QUERY"
const val SIMILAR = "%s/similar?$QUERY"

const val FIRED = "fired!"