package com.zeyadgasser.challenge.mvi.view

/**
 * @author Zeyad Gasser.
 */
interface ErrorMessageFactory {
    fun getErrorMessage(throwable: Throwable): String
}