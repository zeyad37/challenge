package com.zeyadgasser.challenge.mvi.view

import android.os.Bundle
import android.os.Parcelable
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDelegate
import com.zeyadgasser.challenge.mvi.BaseEvent
import com.zeyadgasser.challenge.mvi.view.BaseView.UI_MODEL
import com.zeyadgasser.challenge.mvi.viewmodel.BaseViewModel
import com.zeyadgasser.challenge.utils.toLiveData
import io.reactivex.Observable

/**
 * @author Zeyad Gasser.
 */
abstract class BaseActivity<S : Parcelable, VM : BaseViewModel<S>> : AppCompatActivity(), LoadDataView<S> {
    lateinit var viewModel: VM
    var viewState: S? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        val isNew = savedInstanceState == null
        if (savedInstanceState != null) {
            BaseView.getViewStateFrom<S>(savedInstanceState)?.let { viewState = it }
        }
        initialize()
        setupUI(isNew)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        BaseView.getViewStateFrom<S>(savedInstanceState)?.let {
            viewState = it
        }
    }

    override fun onSaveInstanceState(bundle: Bundle) {
        bundle.putParcelable(UI_MODEL, viewState)
        super.onSaveInstanceState(bundle)
    }

    override fun onStart() {
        super.onStart()
        viewModel.processEvents(events(), viewState).toLiveData()
                .observe(this, UIObserver<LoadDataView<S>, S>(this, errorMessageFactory()))
    }

    override fun setState(bundle: S) {
        viewState = bundle
    }

    abstract fun errorMessageFactory(): ErrorMessageFactory

    /**
     * Initialize objects or any required dependencies.
     */
    abstract fun initialize()

    /**
     * Setup the UI.
     *
     * @param isNew = savedInstanceState == null
     */
    abstract fun setupUI(isNew: Boolean)

    /**
     * Merge all events into one [Observable].
     *
     * @return [Observable].
     */
    abstract fun events(): Observable<BaseEvent<*>>
}