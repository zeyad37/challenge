package com.zeyadgasser.challenge.mvi

/**
 * @author Zeyad Gasser.
 */
interface BaseEvent<T> {
    fun getPayLoad(): T
}