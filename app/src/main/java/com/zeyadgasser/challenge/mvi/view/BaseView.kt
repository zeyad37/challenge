package com.zeyadgasser.challenge.mvi.view

import android.os.Bundle
import android.os.Parcelable

/**
 * @author Zeyad Gasser.
 */
object BaseView {
    const val UI_MODEL = "viewState"

    fun <S : Parcelable> getViewStateFrom(savedInstanceState: Bundle): S? =
            if (savedInstanceState.containsKey(UI_MODEL)) {
                savedInstanceState.getParcelable(UI_MODEL)
            } else null
}

