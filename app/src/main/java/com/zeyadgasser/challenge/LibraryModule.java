package com.zeyadgasser.challenge;

import io.realm.annotations.RealmModule;

@RealmModule(library = true, allClasses = true)
class LibraryModule {
}